## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Today, we learned about various concepts, including front-end development, cross-domain issues, and back-end integration. The activities involved understanding front-end concepts through concept mapping, learning about cross-domain problem-solving, and completing the back-end code for a to-do list while integrating it with the front-end.

- R (Reflective): Engaging.

- I (Interpretive): Today's class provided valuable insights into both front-end and back-end development aspects. Understanding front-end concepts through concept mapping allowed us to visualize the relationships between different elements, making it easier to grasp the overall structure of web development. Learning about cross-domain issues was crucial as it helps us address challenges related to data sharing and security between different domains. Completing the integration of front-end and back-end for the to-do list was a practical exercise that showcased the importance of seamless collaboration between these two components to create a functional web application.

- D (Decisional): I am eager to apply what I have learned today in real-world web development projects. The knowledge of front-end concepts, cross-domain problem-solving, and back-end integration will help me build more sophisticated and robust web applications. I plan to implement these skills by working on personal projects and collaborating with others on web development tasks. Additionally, I will make changes to my development approach by ensuring better communication and coordination between front-end and back-end teams, as I now understand the significance of their integration for a successful application.