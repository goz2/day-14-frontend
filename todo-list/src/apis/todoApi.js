import axios from 'axios'

const requset = axios.create({
    baseURL: 'http://localhost:8080'
})

export const getTodoList = () => {
    return requset.get('/todos')
}

export const createTodo = (text) => {
    return requset.post('/todos', {
        done: false,
        text
    })
}

export const deleteTodo = (id) => {
    return requset.delete(`/todos/${id}`)
}

export const editTodo = (id, done, text) => {
    return requset.put(`/todos/${id}`, {
        done,
        text
    })
}
export const getTodoById = (id) => {
    return requset.get(`/todos/${id}`)
}

const todoApi = {
    getTodoList,
    createTodo,
    deleteTodo,
    editTodo,
    getTodoById
}

export default todoApi
