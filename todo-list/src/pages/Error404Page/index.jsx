import './index.css'
import {Link} from 'react-router-dom'

function Error404Page() {
    return (
        <div className="main">
            Error 404 Page

            <Link to="/">Back to Home</Link>
        </div>
    )
}

export default Error404Page