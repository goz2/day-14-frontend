import {useNavigate} from 'react-router-dom'
import '../../css/index.css'
import './index.css'
import {Table, Typography} from 'antd'
import todoApi from '../../apis/todoApi'
import {useEffect, useState} from 'react'

function DoneListPage() {
    const [todoList, setTodoList] = useState()
    const fetchData = async () => {
        const {data} = await todoApi.getTodoList()
        const curTodoList = data.filter(todo => todo.done).map(todo => {
            return {
                id: todo.id,
                key: todo.id,
                text: todo.text,
            }
        })
        setTodoList(curTodoList)
    }
    useEffect(() => {
        fetchData()
    }, [])
    const navigate = useNavigate()
    const handleClick = (id) => {
        navigate(`/done/${id}`)
    }
    const columns = [
        {
            title: 'Completed Todos',
            dataIndex: 'text',
            key: 'text',
            render: (text, record) => (
                <Typography.Text onClick={() => handleClick(record.id)} style={{cursor: 'pointer'}}>
                    {text}
                </Typography.Text>
            ),
        },
    ]
    return (
        <div className="curMain">
            <div className="tableContainer">
                <Table
                    columns={columns}
                    dataSource={todoList}
                    pagination={false}
                />
            </div>
        </div>
    )
}

export default DoneListPage