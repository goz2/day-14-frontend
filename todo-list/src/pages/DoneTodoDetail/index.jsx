import {useNavigate, useParams} from 'react-router-dom'
import '../../css/index.css'
import todoApi from '../../apis/todoApi'
import {useEffect, useState} from 'react'

function DoneTodoDetail() {
    const {id} = useParams()
    const [todo, setTodo] = useState()
    const navigate = useNavigate()
    const getTodoById = async () => {
        try {
            const {data} = await todoApi.getTodoById(id)
            setTodo(data)
        } catch (err) {
            alert(err.response.data.message)
            navigate('404')
        }
    }

    useEffect(() => {
        getTodoById()
    }, [])
    return (
        <div className="flexMain">
            id: {id}
            <br/>
            todo: {todo?.text}
        </div>
    )
}

export default DoneTodoDetail