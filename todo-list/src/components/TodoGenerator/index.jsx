import {useState} from 'react'
import './index.css'
import useTodo from '../../hooks/todo.hook'
import {Input, Button} from 'antd'
import _ from 'lodash'

function TodoGenetator() {
    const [text, setText] = useState('')
    const {createTodo} = useTodo()
    const handleAdd = _.debounce(async () => {
        if (text && text.trim().length > 0) {
            await createTodo(text)
            setText('')
        }
    }, 500)

    return (
        <div className="contentMain">
            <Input type="text" onChange={e => setText(e.target.value)} value={text}/>
            <Button className="button" type="primary" onClick={handleAdd}>ADD</Button>
        </div>
    )
}

export default TodoGenetator