import {Link} from 'react-router-dom'
import '../../css/index.css'
import React, {useState} from 'react';

import {Menu} from 'antd'

const items = [
    {
        label: (<Link to={'/'}> Home Page</Link>),
        key: 'index',
    },
    {
        label: (<Link to={'/done'}> Done Page</Link>),
        key: 'done',
    },
    {
        label: (<Link to={'/help'}> Help Page</Link>),
        key: 'help',
    },
]

function Navigator() {
    const [current, setCurrent] = useState('index')
    const onClick = (e) => {
        setCurrent(e.key)
    }
    return (
        <div className="flexMain">
            <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items}/>
        </div>
    )
}

export default Navigator