import {useDispatch} from 'react-redux'
import {loadTodoList} from '../slices/todoListSlice'
import todoApi from '../apis/todoApi'

const useTodo = () => {
    const dispatch = useDispatch()

    const getTodoList = async () => {
        const {data} = await todoApi.getTodoList()
        dispatch(loadTodoList(data))
    }

    const createTodo = async (text) => {
        await todoApi.createTodo(text)
        await getTodoList()
    }

    const editTodo = async (id, done, text) => {
        await todoApi.editTodo(id, done, text)
        await getTodoList()
    }

    const deleteTodo = async (id) => {
        await todoApi.deleteTodo(id)
        await getTodoList()
    }

    return {
        getTodoList,
        createTodo,
        editTodo,
        deleteTodo,
    }
}

export default useTodo